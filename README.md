# matekarte

## Dependencies

* [OpenStreetMap](http://www.openstreetmap.org/)
* [Leaflet](https://leafletjs.com)
* [Leaflet Control Geocoder](https://github.com/perliedman/leaflet-control-geocoder)
* [Leaflet Locate](https://github.com/domoritz/leaflet-locatecontrol)
* [Leaflet Hash](https://github.com/mlevans/leaflet-hash)
* [Leaflet markercluster](https://github.com/Leaflet/Leaflet.markercluster)
* Leaflet Infobox
* [Club Mate Mapping Guideline](https://github.com/cccmzwi/matekate)
* [Font Awesome](http://fontawesome.io/)

